CFLAGS=-O2 -Wall -pedantic
INC=
DBG=-g -DDEBUG
LDFLAGS=-lX11 -lXrandr -lm
EXE=rotate_screen
SRC=$(EXE).c

$(EXE): 
	$(CC) $(CFLAGS) $(LDFLAGS) -o $(EXE) $(SRC)


.PHONY: debug
debug: CFLAGS+=$(DBG)
debug:
	$(CC) $(CFLAGS) $(LDFLAGS) -o $(EXE)-dbg $(SRC)


.PHONY: all
all: $(EXE) build-tests


.PHONY: clean
clean:
	rm -f $(EXE) $(EXE)-dbg


##########################
# Testing infrastructure #
##########################

.PHONY: run-tests
run-tests: build-tests
	for TEST in $(subst tests/,,$(TEST_EXE)) ; do \
	    echo "Running $$TEST"; \
	    (cd tests && ./$$TEST); \
	done;

tests/%: tests/%.o $(OBJ_WITHOUT_MAIN)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $< $(OBJ_WITHOUT_MAIN)

.PHONY: build-tests
build-tests: $(TEST_EXE)

#################################
# Development environment setup #
# ###############################

# main rule for setting up the dev environment with Vim
.PHONY: setup_envdev
setup_envdev: bear types

# create a compile_ommands.json file for autocompletion in Vim, requires bear
.PHONY: bear
bear: clean
	bear -- make all

# Make a highlight file for types.  Requires Exuberant ctags and awk
.PHONY: types
types: types.vim
types.vim: $(SRC) $(HEADERS)
	ctags --c-kinds=gstu -o- $(SRC) $(HEADERS) |\
	    awk 'BEGIN{printf("syntax keyword Type\t")}\
	        {printf("%s ", $$1)}END{print ""}' > $@
