/* This program is based on xrandr code.
 *
 * Below is reproduced the content of the copyright header:
 *
 * Copyright © 2001 Keith Packard, member of The XFree86 Project, Inc.
 * Copyright © 2002 Hewlett Packard Company, Inc.
 * Copyright © 2006 Intel Corporation
 * Copyright © 2013 NVIDIA Corporation
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that copyright
 * notice and this permission notice appear in supporting documentation, and
 * that the name of the copyright holders not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  The copyright holders make no representations
 * about the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 *
 * THE COPYRIGHT HOLDERS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 * DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 *
 * Thanks to Jim Gettys who wrote most of the client side code,
 * and part of the server code for randr.
 */

#include <stdio.h>
#include <X11/Xlib.h>
#include <X11/extensions/Xrandr.h>
#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdarg.h>
#include <math.h>

static char	*program_name;
static Display	*dpy;
static Window	root;
static int	screen = -1;

static void
usage(void)
{
    printf("usage: %s [option]\n%s", program_name,
           "  where option is:\n"
           "  --help\n"
	   "  --clockwise\n"
	   "  		or --cw\n"
	   "  --counter-clockwise\n"
	   "  		or --ccw\n");
}

static void _X_NORETURN _X_ATTRIBUTE_PRINTF(1,2)
argerr (const char *format, ...)
{
    va_list ap;

    va_start (ap, format);
    fprintf (stderr, "%s: ", program_name);
    vfprintf (stderr, format, ap);
    fprintf (stderr, "Try '%s --help' for more information.\n", program_name);
    va_end (ap);
    exit (1);
    /*NOTREACHED*/
}

int
main (int argc, char **argv)
{
    XRRScreenConfiguration *sc;
    Status	status = RRSetConfigFailed;
    Rotation	current_rotation;
    char       *display_name = NULL;
    double    	rate = -1;
    int		size = -1;
    int		ret = 0;
    Bool 	rotate_cw = False;

    program_name = argv[0];
    for (int i = 1; i < argc; i++) {
	if (!strcmp("-help", argv[i]) || !strcmp("--help", argv[i])) {
	    usage();
	    exit(0);
	}
	if (!strcmp("--clockwise", argv[i]) || !strcmp("--cw", argv[i])) {
	    //rotation = (1 << (current_rotation + 1)) & 0xf;
	    rotate_cw = True;
	    continue;
	}
	if (!strcmp("--counter-clockwise", argv[i]) || !strcmp("--ccw", argv[i])) {
	    //rotation = (1 << (current_rotation + 1)) & 0xf;
	    rotate_cw = False;
	    continue;
	}

	argerr ("unrecognized option '%s'\n", argv[i]);
    }

    dpy = XOpenDisplay (display_name);

    if (dpy == NULL) {
	fprintf (stderr, "Can't open display %s\n", XDisplayName(display_name));
	exit (1);
    }
    if (screen < 0)
	screen = DefaultScreen (dpy);
    if (screen >= ScreenCount (dpy)) {
	fprintf (stderr, "Invalid screen number %d (display has %d)\n",
		 screen, ScreenCount (dpy));
	exit (1);
    }

    root = RootWindow (dpy, screen);
    sc = XRRGetScreenInfo (dpy, root);

    if (sc == NULL) 
	exit (1);

    size = XRRConfigCurrentConfiguration (sc, &current_rotation);

    rate = XRRConfigCurrentRate (sc);

    /* we should test configureNotify on the root window */
    XSelectInput (dpy, root, StructureNotifyMask);

    XRRSelectInput (dpy, root, RRScreenChangeNotifyMask);
    
    Rotation rotation = rotate_cw ? (current_rotation >> 1) & 0xf : (current_rotation << 1) & 0xf;

    if (rotation == 0) 
         rotation = rotate_cw ? (1 << 3) & 0xf : 1; 
    
    printf("DBG: Current rotation = %X, new rotation = %X\n", current_rotation, rotation);
    status = XRRSetScreenConfigAndRate (dpy, sc, root, (SizeID) size,
    				    (Rotation) rotation,
    				    rate, CurrentTime);

    if (status == RRSetConfigFailed) {
	printf ("Failed to change the screen configuration!\n");
	ret = 1;
    }

    XRRFreeScreenConfigInfo(sc);
    return(ret);
}
